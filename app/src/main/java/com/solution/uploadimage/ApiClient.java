package com.solution.uploadimage;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Android on 2/17/2018.
 */

public class ApiClient {

    private static final String BaseUrl = "https://cdn.ktbfuso.id/";
    private static Retrofit retrofit;

    public static Retrofit getApiClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS).build();
        retrofit = new Retrofit.Builder().baseUrl(BaseUrl).
                client(client).
                addConverterFactory(GsonConverterFactory.create()).build();

        return retrofit;
    }
}

