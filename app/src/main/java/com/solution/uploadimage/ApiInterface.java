package com.solution.uploadimage;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Android on 2/17/2018.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("uploader")
    Call<Img_Pojo> uploadImage(@Field("bucket") String bucket,
                               @Field("path") String path,
                               @Field("file_name") String fileName,
                               @Field("file") String file);
}

